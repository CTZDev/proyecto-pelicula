<?php include("modulos/pelicula.php") ?>

<?php include("cabecera.php"); ?>
<?php include("sidebar.php"); ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Administración de Peliculas</h1>
                </div>
                <!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item">
                            <a href="Vistapanel.php">Inicio</a>
                        </li>
                        <li class="breadcrumb-item active">Consulta de Peliculas</li>
                    </ol>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- Small boxes (Stat box) ELIMINADO -->

            <!-- /.row -->

            <!--Formulario de productos ELIMINADO-->
            <!--Fin de formulario de productos-->

            <!-- Main row-->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Listado Total de Peliculas</h3>

                            <div class="card-tools">
                                <div class="input-group input-group-sm">
                                    <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

                                    <div class="input-group-append">
                                        <button type="submit" class="btn btn-default">
                                            <i class="fas fa-search"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body table-responsive p-0" style="height: 600px;">
                            <table class="table table-head-fixed text-nowrap">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Pelicula</th>
                                        <th>Calidad</th>
                                        <th>País de Origen</th>
                                        <th>Idioma</th>
                                        <th>Fecha Estreno</th>
                                        <th>Fecha Registro</th>
                                        <th>Sinópsis</th>
                                        <th>Duracion</th>
                                        <th>Tipo de Participante / Participante</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>Bob Esponja</td>
                                        <td>HD</td>
                                        <td>EE.UU</td>
                                        <td>Español Hispano</td>
                                        <td>14/08/2020</td>
                                        <td>15/08/2020</td>
                                        <td style="max-width:320px; white-space: normal;">
                                            Bob
                                            Esponja y Patricio se embarcan en una aventura épica. Durante una misión
                                            heroica e hilarante para salvar a la mascota de Bob, el caracol Gary,
                                            descubren que nada es más fuerte que el poder de la amistad.
                                        </td>
                                        <td>1hr 15 min</td>
                                        <td style="max-width:150px; white-space: normal;">Actor :Keenu Reves , Snoop
                                            Doog , Stephen Hillenburg / Director : Arminda Reuntis
                                        </td>
                                        <td>
                                            <form action="" method="POST" class="m-0">
                                                <input type="hidden" name="txtNameUser">
                                                <button type="submit" class="btn btn-warning" value="btnModificar" name="action">Modificar</button>
                                                <button type="submit" class="btn btn-danger" value="btnEliminar" name="action">Eliminar</button>
                                            </form>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>Mulan</td>
                                        <td>HD</td>
                                        <td>Japon</td>
                                        <td>Español Hispano</td>
                                        <td>20/09/2020</td>
                                        <td>22/09/2020</td>
                                        <td style="max-width:320px; white-space: normal;">
                                            El emperador chino emite un decreto que exige reclutar a un varón de cada
                                            familia para luchar con el ejército imperial. Para salvar a su anciano padre
                                            de este deber, su única hija Fa Mulán se hace pasar por soldado y toma su
                                            lugar.
                                        </td>
                                        <td>1hr 30 min</td>
                                        <td style="max-width:150px; white-space: normal;">Actor :Jet Li , Donnie Yeng
                                            Ming Na Ween / Director : Alaska Matheuws
                                        </td>
                                        <td>
                                            <form action="" method="POST" class="m-0">
                                                <input type="hidden" name="txtNameUser">
                                                <button type="submit" class="btn btn-warning" value="btnModificar" name="action">Modificar</button>
                                                <button type="submit" class="btn btn-danger" value="btnEliminar" name="action">Eliminar</button>
                                            </form>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td>Los Mutantes</td>
                                        <td>FULL HD</td>
                                        <td>EE.UU</td>
                                        <td>Español Hispano</td>
                                        <td>15/08/2020</td>
                                        <td>17/08/2020</td>
                                        <td style="max-width:320px; white-space: normal;">
                                            Cinco jóvenes mutantes, retenidos contra su voluntad en una instalación
                                            secreta, luchan por escapar de sus pecados cometidos en el pasado.
                                        </td>
                                        <td>1hr 45 min</td>
                                        <td style="max-width:150px; white-space: normal;">Actor :Ana Taylor, Henry Zaga
                                            , Charlie Heathon / Director : Massie Williams
                                        </td>
                                        <td>
                                            <form action="" method="POST" class="m-0">
                                                <input type="hidden" name="txtNameUser">
                                                <button type="submit" class="btn btn-warning" value="btnModificar" name="action">Modificar</button>
                                                <button type="submit" class="btn btn-danger" value="btnEliminar" name="action">Eliminar</button>
                                            </form>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>4</td>
                                        <td>La Mascara</td>
                                        <td>HD</td>
                                        <td>EE.UU</td>
                                        <td>Español Hispano</td>
                                        <td>25/01/1996</td>
                                        <td>15/08/2020</td>
                                        <td style="max-width:320px; white-space: normal;">
                                            Una máscara antigua transforma a un monótono empleado bancario en un Romeo
                                            sonriente con poderes sobrehumanos.
                                        </td>
                                        <td>1hr 15 min</td>
                                        <td style="max-width:150px; white-space: normal;">Actor :Jim Carrey , Cameron
                                            Diaz , Peter Greene / Director : Amy Yasbeck
                                        </td>
                                        <td>
                                            <form action="" method="POST" class="m-0">
                                                <input type="hidden" name="txtNameUser">
                                                <button type="submit" class="btn btn-warning" value="btnModificar" name="action">Modificar</button>
                                                <button type="submit" class="btn btn-danger" value="btnEliminar" name="action">Eliminar</button>
                                            </form>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
            <!-- /.row (main row) -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->


<!--FOOTER-->
<?php include("footer.php") ?>

<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
</aside>
<!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge("uibutton", $.ui.button);
</script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- ChartJS -->
<script src="plugins/chart.js/Chart.min.js"></script>
<!-- Sparkline -->
<!-- <script src="plugins/sparklines/sparkline.js"></script> ELIMINADO-->
<!-- JQVMap -->
<!-- <script src="plugins/jqvmap/jquery.vmap.min.js"></script> ELIMINADO-->
<!-- <script src="plugins/jqvmap/maps/jquery.vmap.usa.js"></script>ELIMINADO -->
<!-- jQuery Knob Chart -->
<script src="plugins/jquery-knob/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="plugins/moment/moment.min.js"></script>
<script src="plugins/daterangepicker/daterangepicker.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Summernote -->
<script src="plugins/summernote/summernote-bs4.min.js"></script>
<!-- overlayScrollbars -->
<script src="plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.js"></script>
<!-- <script src="dist/js/demo.js"></script> ELIMINADO -->
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!-- <script src="dist/js/pages/dashboard.js"></script> -->
<!--jquery validation-->
<script src="plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="plugins/jquery-validation/additional-methods.min.js"></script>
<script>
    document.getElementById("pelicula-menu").className = "nav-item menu-open";
    document.getElementById("adminpelicula").className = "nav-link active";
    document.getElementById("lpelicula").className = "nav-link active";
    $(function() {
        $.validator.setDefaults({
            submitHandler: function() {
                alert("Form successful submitted!");
            }
        });
        $('#quickForm').validate({
            rules: {
                txtNameUser: {
                    required: true,
                },

                txtEmailUser: {
                    required: true,
                    email: true,
                },
                txtPassword: {
                    required: true,
                    minlength: 9
                },
                chktermsUser: {
                    required: true
                },
            },
            messages: {
                txtNameUser: {
                    required: "Porfavor ingresa tu nombre"
                },
                txtEmailUser: {
                    required: "Porfavor ingresa un correo electronico",
                    email: "Porfavor ingresa un correo electronico valido"
                },
                txtPassword: {
                    required: "Porfavor ingresa tu contraseña",
                    minlength: "Tu contraseña debe tener al menos 9 caracteres"
                },
                chktermsUser: "Porfavor acepta nuestros términos"
            },
            errorElement: 'span',
            errorPlacement: function(error, element) {
                error.addClass('invalid-feedback');
                element.closest('.form-group').append(error);
            },
            highlight: function(element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).removeClass('is-invalid');
            }
        });
    });
</script>
</body>

</html>