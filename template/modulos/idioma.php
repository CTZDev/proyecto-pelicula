<?php
include('global/sesiones.php');
include('global/conexion.php');

// echo "Soy idioma en modulos";

$txtID = (isset($_POST["txtID"]))
    ? $_POST["txtID"]
    : "";

$txtName =
    (isset($_POST["txtName"]))
    ? $_POST["txtName"]
    : "";

$option = (isset($_POST["option"]))
    ? $_POST["option"]
    : "";

$accionAgregar = "";
$accionModificar = $accionCancelar = "disabled";

switch ($option) {
    case "btnAgregar":

        $query = "INSERT INTO idioma (nombre_lenguaje , estado)
                VALUES (:nombre_lenguaje , 1)";
        $sql = $pdo->prepare($query);
        $sql->bindParam(':nombre_lenguaje', $txtName);
        $sql->execute();
        header('Location: Vistaidioma.php');
        echo "Presionaste AGREGAR";
        break;

    case "btnModificar":
        $query = "UPDATE idioma SET nombre_lenguaje=:nombre_lenguaje WHERE ididioma=:ididioma";
        $sql = $pdo->prepare($query);
        $sql->bindParam(':ididioma', $txtID); //FALTA DEFINIR VARIABLE
        $sql->bindParam(':nombre_lenguaje', $txtName);
        $sql->execute();
        header('Location: Vistaidioma.php');
        echo "Presionaste MODIFICAR";
        break;

    case "btnEliminar":

        $query = "DELETE FROM idioma WHERE ididioma = :ididioma";
        $sql = $pdo->prepare($query);
        $sql->bindParam(':ididioma', $txtID); //FALTA DEFINIR VARIABLE
        $sql->execute();
        header('Location: Vistaidioma.php');
        echo "Presionaste ELIMINAR";
        break;

    case "btnCancelar":
        header('Location: Vistaidioma.php');
        break;

    case "Seleccionar Registro":
        $accionAgregar = "disabled";
        $accionModificar = $accionCancelar = "";
        break;
}

$query = "SELECT * FROM idioma";
$sql = $pdo->prepare($query);
$sql->execute();
$data = $sql->fetchAll(PDO::FETCH_ASSOC);

// print_r($data);