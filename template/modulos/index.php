<?php

if (isset($_POST["btnLogin"])) {
    //INSTANCIAR CONEXION A BD
    include("global/conexion.php");
    //CAPTURAR INPUT
    $txtEmail = ($_POST["txtEmail"]);
    $txtPassword = ($_POST["txtPassword"]);
    //CONSULTA A BD
    $sentenciaSQL = $pdo->prepare("SELECT * FROM usuario 
                                    WHERE email=:email 
                                    AND password=:password");
    //SENTENCIA PASANDO PARAMETROS EN STRING
    $sentenciaSQL->bindParam("email", $txtEmail, PDO::PARAM_STR);
    $sentenciaSQL->bindParam("password", $txtPassword, PDO::PARAM_STR);
    //EJECUCION
    $sentenciaSQL->execute();
    //INFO DE USUARIO EN FORMA DE ARREGLO
    $registro = $sentenciaSQL->fetch(PDO::FETCH_ASSOC);

    // print_r($registro);

    //CONTAR FILAS DE CONSULTA
    $numeroRegistros = $sentenciaSQL->rowCount();

    if ($numeroRegistros >= 1) {
        //COMIENZA SESION
        session_start();
        // VARIABLE DE SESION Y ALMACENAMOS INFO
        $_SESSION['usuario'] = $registro;
        echo "Bienvenido ...";
        //REDIRECCIONAR A UNA PAGINA
        header('Location:Vistapanel.php');
    } else {
        echo "No se encontro registros";
    }
}
