<?php
include('global/sesiones.php');
include('global/conexion.php');

// echo "Soy participante en modulos";

$txtID = (isset($_POST["txtID"]))
    ? $_POST["txtID"]
    : "";

$txtName =
    (isset($_POST["txtName"]))
    ? $_POST["txtName"]
    : "";

$txtLastName =
    (isset($_POST["txtLastName"]))
    ? $_POST["txtLastName"]
    : "";

$option = (isset($_POST["option"]))
    ? $_POST["option"]
    : "";

$accionAgregar = "";
$accionModificar = $accionCancelar = "disabled";

switch ($option) {
    case "btnAgregar":
        $query = "INSERT INTO participante (nombres , apellidos, estado)
                VALUES (:nombres ,:apellidos, 1)";
        $sql = $pdo->prepare($query);
        $sql->bindParam(':nombres', $txtName);
        $sql->bindParam(':apellidos', $txtLastName);
        $sql->execute();
        header('Location: Vistaparticipante.php');
        echo "Presionaste AGREGAR";
        break;

    case "btnModificar":
        $query = "UPDATE participante
            SET nombres=:nombres, apellidos=:apellidos
            WHERE idparticipante=:idparticipante";
        $sql = $pdo->prepare($query);
        $sql->bindParam(':idparticipante', $txtID); //FALTA DEFINIR VARIABLE
        $sql->bindParam(':nombres', $txtName);
        $sql->bindParam(':apellidos', $txtLastName);
        $sql->execute();
        header('Location: Vistaparticipante.php');
        echo "Presionaste MODIFICAR";
        break;

    case "btnEliminar":
        $query = "DELETE FROM participante WHERE idparticipante = :idparticipante";
        $sql = $pdo->prepare($query);
        $sql->bindParam(':idparticipante', $txtID); //FALTA DEFINIR VARIABLE
        $sql->execute();
        header('Location: Vistaparticipante.php');
        echo "Presionaste ELIMINAR";
        break;

    case "btnCancelar":
        header('Location: Vistaparticipante.php');
        break;

    case "Seleccionar Registro":
        $accionAgregar = "disabled";
        $accionModificar = $accionCancelar = "";
        break;
}

$query = "SELECT * FROM participante";
$sql = $pdo->prepare($query);
$sql->execute();
$data = $sql->fetchAll(PDO::FETCH_ASSOC);

// print_r($data);