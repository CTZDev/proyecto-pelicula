<?php
include('global/sesiones.php');
include('global/conexion.php');

// echo "Soy genero en modulos";

$txtID = (isset($_POST["txtID"]))
    ? $_POST["txtID"]
    : "";

$txtName =
    (isset($_POST["txtName"]))
    ? $_POST["txtName"]
    : "";

$option = (isset($_POST["option"]))
    ? $_POST["option"]
    : "";

$accionAgregar = "";
$accionModificar = $accionCancelar = "disabled";

switch ($option) {
    case "btnAgregar":

        $query = "INSERT INTO genero (nombre , estado)
                VALUES (:nombre , 1)";
        $sql = $pdo->prepare($query);
        $sql->bindParam(':nombre', $txtName);
        $sql->execute();
        header('Location: Vistagenero.php');

        echo "Presionaste AGREGAR";
        break;

    case "btnModificar":
        $query = "UPDATE genero SET nombre=:nombre WHERE idgenero=:idgenero";
        $sql = $pdo->prepare($query);
        $sql->bindParam(':idgenero', $txtID); //FALTA DEFINIR VARIABLE
        $sql->bindParam(':nombre', $txtName);
        $sql->execute();
        header('Location: Vistagenero.php');
        echo "Presionaste MODIFICAR";
        break;

    case "btnEliminar":

        $query = "DELETE FROM genero WHERE idgenero = :idgenero";
        $sql = $pdo->prepare($query);
        $sql->bindParam(':idgenero', $txtID); //FALTA DEFINIR VARIABLE
        $sql->execute();
        header('Location: Vistagenero.php');
        echo "Presionaste ELIMINAR";
        break;

    case "btnCancelar":
        header('Location: Vistagenero.php');
        break;

    case "Seleccionar Registro":
        $accionAgregar = "disabled";
        $accionModificar = $accionCancelar = "";
        break;
}

$query = "SELECT * FROM genero";
$sql = $pdo->prepare($query);
$sql->execute();
$data = $sql->fetchAll(PDO::FETCH_ASSOC);

// print_r($data);