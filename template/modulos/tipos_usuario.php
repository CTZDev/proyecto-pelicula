<?php
include('global/sesiones.php');
include('global/conexion.php');

// echo "Soy tipo de usuarios en modulos";

$txtID = (isset($_POST["txtID"]))
    ? $_POST["txtID"]
    : "";

$txtName =
    (isset($_POST["txtName"]))
    ? $_POST["txtName"]
    : "";

$option = (isset($_POST["option"]))
    ? $_POST["option"]
    : "";

$accionAgregar = "";
$accionModificar = $accionCancelar = "disabled";

switch ($option) {
    case "btnAgregar":

        $query = "INSERT INTO tipo_usuario (nombre , estado)
                VALUES (:nombre , 1)";
        $sql = $pdo->prepare($query);
        $sql->bindParam(':nombre', $txtName);
        $sql->execute();
        header('Location: Vistatipos_usuario.php');

        echo "Presionaste AGREGAR";
        break;

    case "btnModificar":
        $query = "UPDATE tipo_usuario SET nombre=:nombre WHERE idtipo_usuario=:idtipo_usuario";
        $sql = $pdo->prepare($query);
        $sql->bindParam(':idtipo_usuario', $txtID); //FALTA DEFINIR VARIABLE
        $sql->bindParam(':nombre', $txtName);
        $sql->execute();
        header('Location: Vistatipos_usuario.php');
        echo "Presionaste MODIFICAR";
        break;

    case "btnEliminar":

        $query = "DELETE FROM tipo_usuario WHERE idtipo_usuario = :idtipo_usuario";
        $sql = $pdo->prepare($query);
        $sql->bindParam(':idtipo_usuario', $txtID); //FALTA DEFINIR VARIABLE
        $sql->execute();
        header('Location: Vistatipos_usuario.php');

        echo "Presionaste ELIMINAR";
        break;

    case "btnCancelar":
        header('Location: Vistatipos_usuario.php');
        break;

    case "Seleccionar Registro":
        $accionAgregar = "disabled";
        $accionModificar = $accionCancelar = "";
        break;
}

$query = "SELECT * FROM tipo_usuario";
$sql = $pdo->prepare($query);
$sql->execute();
$data = $sql->fetchAll(PDO::FETCH_ASSOC);

// print_r($data);