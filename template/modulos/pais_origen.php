<?php
include('global/sesiones.php');
include('global/conexion.php');

// echo "Soy pais de origen en modulos";

$txtID = (isset($_POST["txtID"]))
    ? $_POST["txtID"]
    : "";

$txtName =
    (isset($_POST["txtName"]))
    ? $_POST["txtName"]
    : "";

$option = (isset($_POST["option"]))
    ? $_POST["option"]
    : "";

$accionAgregar = "";
$accionModificar = $accionCancelar = "disabled";

switch ($option) {
    case "btnAgregar":

        $query = "INSERT INTO pais_origen (nombre , estado)
                VALUES (:nombre , 1)";
        $sql = $pdo->prepare($query);
        $sql->bindParam(':nombre', $txtName);
        $sql->execute();
        header('Location: Vistapais_origen.php');

        echo "Presionaste AGREGAR";
        break;

    case "btnModificar":
        $query = "UPDATE pais_origen SET nombre=:nombre WHERE idpais_origen=:idpais_origen";
        $sql = $pdo->prepare($query);
        $sql->bindParam(':idpais_origen', $txtID); //FALTA DEFINIR VARIABLE
        $sql->bindParam(':nombre', $txtName);
        $sql->execute();
        header('Location: Vistapais_origen.php');
        echo "Presionaste MODIFICAR";
        break;

    case "btnEliminar":

        $query = "DELETE FROM pais_origen WHERE idpais_origen = :idpais_origen";
        $sql = $pdo->prepare($query);
        $sql->bindParam(':idpais_origen', $txtID); //FALTA DEFINIR VARIABLE
        $sql->execute();
        header('Location: Vistapais_origen.php');
        echo "Presionaste ELIMINAR";
        break;

    case "btnCancelar":
        header('Location: Vistapais_origen.php');
        break;

    case "Seleccionar Registro":
        $accionAgregar = "disabled";
        $accionModificar = $accionCancelar = "";
        break;
}

$query = "SELECT * FROM pais_origen";
$sql = $pdo->prepare($query);
$sql->execute();
$data = $sql->fetchAll(PDO::FETCH_ASSOC);

// print_r($data);