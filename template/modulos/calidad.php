<?php
include('global/sesiones.php');
include('global/conexion.php');

// echo "Soy tipo de usuarios en modulos";

$txtID = (isset($_POST["txtID"]))
    ? $_POST["txtID"]
    : "";

$txtName =
    (isset($_POST["txtName"]))
    ? $_POST["txtName"]
    : "";

$txtAbrev =
    (isset($_POST["txtAbrev"]))
    ? $_POST["txtAbrev"]
    : "";

$option = (isset($_POST["option"]))
    ? $_POST["option"]
    : "";

$accionAgregar = "";
$accionModificar = $accionCancelar = "disabled";

switch ($option) {
    case "btnAgregar":

        $query = "INSERT INTO calidad (nombre ,abreviatura, estado)
                VALUES (:nombre , :abreviatura, 1)";
        $sql = $pdo->prepare($query);
        $sql->bindParam(':nombre', $txtName);
        $sql->bindParam(':abreviatura', $txtAbrev);
        $sql->execute();
        header('Location: Vistacalidad.php');
        echo "Presionaste AGREGAR";
        break;

    case "btnModificar":
        $query = "UPDATE calidad SET nombre=:nombre , abreviatura=:abreviatura WHERE idcalidad=:idcalidad";
        $sql = $pdo->prepare($query);
        $sql->bindParam(':idcalidad', $txtID); //FALTA DEFINIR VARIABLE
        $sql->bindParam(':nombre', $txtName);
        $sql->bindParam(':abreviatura', $txtAbrev);
        $sql->execute();
        header('Location: Vistacalidad.php');
        echo "Presionaste MODIFICAR";
        break;

    case "btnEliminar":

        $query = "DELETE FROM calidad WHERE idcalidad = :idcalidad";
        $sql = $pdo->prepare($query);
        $sql->bindParam(':idcalidad', $txtID); //FALTA DEFINIR VARIABLE
        $sql->execute();
        header('Location: Vistacalidad.php');
        echo "Presionaste ELIMINAR";
        break;

    case "btnCancelar":
        header('Location: Vistacalidad.php');
        break;

    case "Seleccionar Registro":
        $accionAgregar = "disabled";
        $accionModificar = $accionCancelar = "";
        break;
}

$query = "SELECT * FROM calidad";
$sql = $pdo->prepare($query);
$sql->execute();
$data = $sql->fetchAll(PDO::FETCH_ASSOC);

// print_r($data);