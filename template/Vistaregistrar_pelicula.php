<?php include("modulos/pelicula.php") ?>

<?php include("cabecera.php"); ?>
<?php include("sidebar.php"); ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Administración de Peliculas</h1>
                </div>
                <!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item">
                            <a href="Vistapanel.php">Inicio</a>
                        </li>
                        <li class="breadcrumb-item active">Registro de Peliculas</li>
                    </ol>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- Small boxes (Stat box) ELIMINADO -->

            <!-- /.row -->

            <!--Formulario de usuarios-->
            <div class="row">
                <div class="col-12 col-xl-6">
                    <!-- jquery validation -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Registro de Peliculas / <small>Movies</small></h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form id="quickFormMovie" novalidate="novalidate">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-12 col-md-6">
                                        <div class="form-group">
                                            <label for="nameMovie">Nombre</label>
                                            <input type="text" name="txtNameMovie" class="form-control" id="nameMovie"
                                                placeholder="Nombre de la Pelicula">
                                        </div>
                                        <div class="form-group">
                                            <label for="sinopsisMovie">Sinopsis</label>
                                            <textarea name="txaSinopsisMovie" id="sinopsisMovie" cols="30" rows="3"
                                                class="form-control" placeholder="Sinopsis de la Pelicula"></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label for="nameGender">Genero(s)</label>
                                            <input type="text" name="txtNameGender" class="form-control" id="nameGender"
                                                placeholder="Genero de la Pelicula">
                                        </div>
                                        <div class="form-group">
                                            <label>Pais de Origen</label>
                                            <select class="form-control">
                                                <option>España</option>
                                                <option>EE.UU</option>
                                                <option>Australia</option>
                                                <option>Dinamarca</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Idioma</label>
                                            <select class="form-control">
                                                <option>Inglés</option>
                                                <option>Epañol</option>
                                                <option>Hispano</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <div class="form-group">
                                            <label>Calidad</label>
                                            <select class="form-control">
                                                <option>FULL HD</option>
                                                <option>HD</option>
                                                <option>720p</option>
                                                <option>360p</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="durationMovie">Duracion</label>
                                            <input type="time" name="txtDurationMovie" class="form-control"
                                                id="durationMovie" placeholder="URL de la Pelicula">
                                        </div>
                                        <div class="form-group">
                                            <label for="premiereMovie">Fecha de Estreno</label>
                                            <input type="date" name="txtPremiereMovie" class="form-control"
                                                id="premiereMovie" placeholder="URL de la Pelicula">
                                        </div>
                                        <div class="form-group">
                                            <label for="urlMovie">Alojamiento</label>
                                            <input type="text" name="txtUrlMovie" class="form-control" id="urlMovie"
                                                placeholder="URL de la Pelicula">
                                        </div>
                                        <div class="form-group row">
                                            <label for="imageMovie">Imagen</label>
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" id="customFile"
                                                    name="txtFileMovie">
                                                <label class="custom-file-label" for="customFile">Subir Imagen</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">
                                    Registrar
                                </button>
                                <button type="submit" class="btn btn-default float-right">
                                    Limpiar
                                </button>
                            </div>
                            <!-- /.card-body -->
                        </form>
                    </div>
                    <!-- /.card -->
                </div>

                <!--modal seleccionar participante-->
                <div class="modal fade" id="modal-participante">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Listado de Participantes</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="card-tools mt-3 d-flex justify-content-center">
                                <div class="input-group input-group-sm" style="max-width: 400px;">
                                    <input type="text" name="table_search" class="form-control float-right"
                                        placeholder="Buscar Participante (Actor o Director)">
                                    <div class="input-group-append">
                                        <button type="submit" class="btn btn-default">
                                            <i class="fas fa-search"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-body">
                                <!-- /.card-header -->
                                <div class="card-body table-responsive p-0" style="height: 485px;">
                                    <table class="table table-head-fixed text-nowrap">
                                        <thead>
                                            <tr>
                                                <th>Seleccione</th>
                                                <th>ID</th>
                                                <th>Nombre</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <input class="form-check-input position-static" type="checkbox"
                                                        value="option1" aria-label="...">
                                                </td>
                                                <td>1</td>
                                                <td>Jim Carrey</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <input class="form-check-input position-static" type="checkbox"
                                                        value="option1" aria-label="...">
                                                </td>
                                                <td>2</td>
                                                <td>Jason Statham</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <input class="form-check-input position-static" type="checkbox"
                                                        value="option1" aria-label="...">
                                                </td>
                                                <td>1</td>
                                                <td>Bruce Willie</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <input class="form-check-input position-static" type="checkbox"
                                                        value="option1" aria-label="...">
                                                </td>
                                                <td>1</td>
                                                <td>Mark Ruffallo</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.card-body -->
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /.modal seleccionar participante-->

                <div class="col-12 col-xl-6">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Listado de Participantes (Actores y Directores)</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body table-responsive p-0" style="height: 485px;">
                            <table class=" table table-head-fixed text-nowrap">
                                <thead>
                                    <tr>
                                        <th>Participante</th>
                                        <th>Rol</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Mark Ruffalo</td>
                                        <td>
                                            <form class="was-validated">
                                                <div class="custom-control custom-checkbox mb-3">
                                                    <input type="checkbox" class="custom-control-input"
                                                        id="customControlValidation1" required>
                                                    <label class="custom-control-label"
                                                        for="customControlValidation1">Actor</label>
                                                </div>
                                                <div class="custom-control custom-checkbox mb-3">
                                                    <input type="checkbox" class="custom-control-input"
                                                        id="customControlValidation2" required>
                                                    <label class="custom-control-label"
                                                        for="customControlValidation2">Director</label>
                                                </div>
                                            </form>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Jason Statham</td>
                                        <td>
                                            <form class="was-validated">
                                                <div class="custom-control custom-checkbox mb-3">
                                                    <input type="checkbox" class="custom-control-input"
                                                        id="customControlValidation3" required>
                                                    <label class="custom-control-label"
                                                        for="customControlValidation3">Actor</label>
                                                </div>
                                                <div class="custom-control custom-checkbox mb-3">
                                                    <input type="checkbox" class="custom-control-input"
                                                        id="customControlValidation4" required>
                                                    <label class="custom-control-label"
                                                        for="customControlValidation4">Director</label>
                                                </div>
                                            </form>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Bruce Willie</td>
                                        <td>
                                            <form class="was-validated">
                                                <div class="custom-control custom-checkbox mb-3">
                                                    <input type="checkbox" class="custom-control-input"
                                                        id="customControlValidation5" required>
                                                    <label class="custom-control-label"
                                                        for="customControlValidation5">Actor</label>
                                                </div>
                                                <div class="custom-control custom-checkbox mb-3">
                                                    <input type="checkbox" class="custom-control-input"
                                                        id="customControlValidation6" required>
                                                    <label class="custom-control-label"
                                                        for="customControlValidation6">Director</label>
                                                </div>
                                            </form>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                        <!--card-footer-->
                        <div class="card-footer">
                            <form id="quickFormCompetitor" novalidate="novalidate" action="" method="POST">
                                <button type="button" class="btn btn-success" data-toggle="modal"
                                    data-target="#modal-participante">
                                    Seleccionar Participante
                                </button>
                            </form>

                        </div>
                        <!--/.card-footer-->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
            <!--Fin de formulario de usuarios-->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!--FOOTER-->
<?php include("footer.php") ?>

<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
</aside>
<!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
$.widget.bridge("uibutton", $.ui.button);
</script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- ChartJS -->
<script src="plugins/chart.js/Chart.min.js"></script>
<!-- Sparkline -->
<!-- <script src="plugins/sparklines/sparkline.js"></script> ELIMINADO-->
<!-- JQVMap -->
<!-- <script src="plugins/jqvmap/jquery.vmap.min.js"></script> ELIMINADO-->
<!-- <script src="plugins/jqvmap/maps/jquery.vmap.usa.js"></script>ELIMINADO -->
<!-- jQuery Knob Chart -->
<script src="plugins/jquery-knob/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="plugins/moment/moment.min.js"></script>
<script src="plugins/daterangepicker/daterangepicker.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Summernote -->
<script src="plugins/summernote/summernote-bs4.min.js"></script>
<!-- overlayScrollbars -->
<script src="plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.js"></script>
<!-- <script src="dist/js/demo.js"></script> ELIMINADO -->
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!-- <script src="dist/js/pages/dashboard.js"></script> -->
<!--jquery validation-->
<script src="plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="plugins/jquery-validation/additional-methods.min.js"></script>
<script>
document.getElementById("pelicula-menu").className = "nav-item menu-open";
document.getElementById("adminpelicula").className = "nav-link active";
document.getElementById("pelicula").className = "nav-link active";
$(function() {
    $.validator.setDefaults({
        submitHandler: function() {
            alert("Form successful submitted!");
        }
    });
    $('#quickFormMovie').validate({
        rules: {
            txtNameMovie: {
                required: true,
            },
            txaSinopsisMovie: {
                required: true,
                minlength: 40,
                maxlength: 150
            },
            txtNameGender: {
                required: true,
            },
            txtDurationMovie: {
                required: true,
            },
            txtPremiereMovie: {
                required: true,
            },
            txtUrlMovie: {
                required: true,
                url: true,
            },
            txtFileMovie: {
                required: true
            },
        },
        messages: {
            txtNameMovie: {
                required: "Porfavor ingresa el nombre de la Pelicula"
            },
            txaSinopsisMovie: {
                required: "Porfavor ingresa la sinopsis",
                minlength: "Porfavor ingresa una sinopsis mayor a 40 caracteres",
                maxlength: "Porfavor ingresa una sinopsis menor a 150 caracteres"
            },
            txtNameGender: {
                required: "Porfavor ingresa el género de la Película",
            },
            txtDurationMovie: {
                required: "Porfavor ingresa la duración de la Película",
            },
            txtPremiereMovie: {
                required: "Porfavor ingresa la fecha de estreno",
            },
            txtUrlMovie: {
                required: "Porfavor ingresa una URL",
                url: "Porfavor Ingresa una URL valida"
            },
            txtFileMovie: {
                required: "Porfavor sube una imagen",
            }
        },
        errorElement: 'span',
        errorPlacement: function(error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function(element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });
});

</script>
</body>

</html>