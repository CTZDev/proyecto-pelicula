<?php include("modulos/usuarios.php") ?>

<?php include("cabecera.php"); ?>
<?php include("sidebar.php"); ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Administración de Usuarios</h1>
                </div>
                <!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item">
                            <a href="Vistapanel.php">Inicio</a>
                        </li>
                        <li class="breadcrumb-item active">Usuarios</li>
                    </ol>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- Small boxes (Stat box) ELIMINADO -->

            <!-- /.row -->

            <!--Formulario de usuarios-->
            <div class="row">
                <div class="col-md-4">
                    <!-- jquery validation -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Registro de Usuarios / <small>User</small></h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form id="quickForm" novalidate="novalidate">
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="nameUser">Usuario</label>
                                    <input type="text" name="txtNameUser" class="form-control" id="nameUser"
                                        placeholder="Nombre o Apodo">
                                </div>
                                <div class="form-group">
                                    <label>Tipo de Usuario</label>
                                    <select class="form-control">
                                        <option>Asistente</option>
                                        <option>Administrador</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="emailUser">Correo electrónico</label>
                                    <input type="email" name="txtEmailUser" class="form-control" id="emailUser"
                                        placeholder="Ingresa tu correo electrónico">
                                </div>
                                <div class="form-group">
                                    <label for="passwordUser">Password</label>
                                    <input type="password" name="txtPassword" class="form-control" id="passwordUser"
                                        placeholder="Contraseña">
                                </div>
                                <div class="form-group mb-0">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" name="chktermsUser" class="custom-control-input"
                                            id="termsUser">
                                        <label class="custom-control-label" for="termsUser">Acepto los <a
                                                href="#">terminos y condiciones</a>.</label>
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Agregar</button>
                                <button type="submit" class="btn btn-default float-right">
                                    Limpiar
                                </button>
                            </div>
                        </form>
                    </div>
                    <!-- /.card -->
                </div>
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Listado de usuarios</h3>

                            <div class="card-tools">
                                <div class="input-group input-group-sm" style="width: 150px;">
                                    <input type="text" name="table_search" class="form-control float-right"
                                        placeholder="Search">

                                    <div class="input-group-append">
                                        <button type="submit" class="btn btn-default">
                                            <i class="fas fa-search"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body table-responsive p-0" style="height: 485px;">
                            <table class=" table table-head-fixed text-nowrap">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Nombre</th>
                                        <th>Tipo de Usuario</th>
                                        <th>Correo electronico</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>Carlos Teran</td>
                                        <td>Administrador</td>
                                        <td><span class=" tag tag-success">carlosteranzavaleta@hotmail.com</span></td>
                                        <td>
                                            <form action="" method="POST" class="m-0">
                                                <input type="hidden" name="txtNameUser">
                                                <button type="submit" class="btn btn-warning" value="btnModificar"
                                                    name="action">Modificar</button>
                                                <button type="submit" class="btn btn-danger" value="btnEliminar"
                                                    name="action">Eliminar</button>
                                            </form>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>Jose Menacho</td>
                                        <td>Administrador</td>
                                        <td><span class="tag tag-success">josemenacho@hotmail.com</span></td>
                                        <td>
                                            <form action="" method="POST" class="m-0">
                                                <input type="hidden" name="txtNameUser">
                                                <button type="submit" class="btn btn-warning" value="btnModificar"
                                                    name="action">Modificar</button>
                                                <button type="submit" class="btn btn-danger" value="btnEliminar"
                                                    name="action">Eliminar</button>
                                            </form>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
            <!--Fin de formulario de usuarios-->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!--FOOTER-->
<?php include("footer.php") ?>
<script>
    document.getElementById("usuario").className = "nav-link active";
</script>
<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
</aside>
<!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
$.widget.bridge("uibutton", $.ui.button);
</script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- ChartJS -->
<script src="plugins/chart.js/Chart.min.js"></script>
<!-- Sparkline -->
<!-- <script src="plugins/sparklines/sparkline.js"></script> ELIMINADO-->
<!-- JQVMap -->
<!-- <script src="plugins/jqvmap/jquery.vmap.min.js"></script> ELIMINADO-->
<!-- <script src="plugins/jqvmap/maps/jquery.vmap.usa.js"></script>ELIMINADO -->
<!-- jQuery Knob Chart -->
<script src="plugins/jquery-knob/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="plugins/moment/moment.min.js"></script>
<script src="plugins/daterangepicker/daterangepicker.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Summernote -->
<script src="plugins/summernote/summernote-bs4.min.js"></script>
<!-- overlayScrollbars -->
<script src="plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.js"></script>
<!-- <script src="dist/js/demo.js"></script> ELIMINADO -->
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!-- <script src="dist/js/pages/dashboard.js"></script> -->
<!--jquery validation-->
<script src="plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="plugins/jquery-validation/additional-methods.min.js"></script>
<script>
$(function() {
    $.validator.setDefaults({
        submitHandler: function() {
            alert("Form successful submitted!");
        }
    });
    $('#quickForm').validate({
        rules: {
            txtNameUser: {
                required: true,
            },

            txtEmailUser: {
                required: true,
                email: true,
            },
            txtPassword: {
                required: true,
                minlength: 9
            },
            chktermsUser: {
                required: true
            },
        },
        messages: {
            txtNameUser: {
                required: "Porfavor ingresa tu nombre",
            },
            txtEmailUser: {
                required: "Porfavor ingresa un correo electronico",
                email: "Porfavor ingresa un correo electronico valido"
            },
            txtPassword: {
                required: "Porfavor ingresa tu contraseña",
                minlength: "Tu contraseña debe tener al menos 9 caracteres"
            },
            chktermsUser: "Porfavor acepta nuestros términos"
        },
        errorElement: 'span',
        errorPlacement: function(error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function(element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });
});
</script>
</body>

</html>