<?php include("modulos/index.php") ?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>CN&Movie</title>

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
    <link rel="stylesheet" href="plugins/icheck-bootstrap/icheck-bootstrap.min.css">
    <link rel="stylesheet" href="dist/css/adminlte.min.css">
    <style>
        .login-page {
            max-width: 700px;
            padding: 15px;
            margin: auto;
        }

        .login-box {
            width: 550px;
        }

        .login-card-body {
            padding: 40px;
            background-color: #E9ECEF;
            box-shadow: 2px 1px 6px 1px;
        }

        #brand {
            filter: brightness(1.1);
            mix-blend-mode: multiply;
        }

        button[name="btnLogin"] {
            background-color: #2e054d;
            color: #fff;
            padding: .5rem;
            transition: all 150ms ease-in-out;
        }


        button[name="btnLogin"]:hover {
            color: #E9ECEF;
            transform: scale(1.005);
            font-weight: bold;
        }


        input {
            padding: 1.35rem !important;
        }
    </style>
</head>

<body class="hold-transition login-page">
    <div class="login-box">
        <div class="login-logo">
            <a href="#">
                <img class="mb-4" src="../template/dist/img/cnMovie.png" alt="Logo de la empresa" width="550" height="250" id="brand" />
            </a>
            <h1 class="h3 mb-5 fw-normal" style="color: #2e054d">Iniciar Sesión</h1>
        </div>
        <!-- /.login-logo -->
        <div class="card">
            <div class="card-body login-card-body">
                <p class="login-box-msg" style="color: 2e054d; font-size: 1.15rem; margin-bottom: 1.25rem;">Ingresa tu email y contraseña</p>
                <form action="index.php" method="post">
                    <div class="input-group mb-3">
                        <input type="email" name="txtEmail" class="form-control" placeholder="Email:example@com" style>
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-envelope"></span>
                            </div>
                        </div>
                    </div>
                    <div class="input-group mb-4">
                        <input type="password" name="txtPassword" class="form-control" placeholder="Password">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <button type="submit" name="btnLogin" class="btn btn-block">Iniciar sesión</button>
                        </div>
                    </div>
                    <p class="mt-5 mb-3 text-muted" style="text-align: center;">CN&MOVIE &copy; 2022</p>
                </form>
            </div>
        </div>
    </div>

    <!-- jQuery -->
    <script src="plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/adminlte.min.js"></script>
</body>

</html>