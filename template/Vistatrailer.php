<?php include("modulos/trailer.php") ?>

<?php include("cabecera.php"); ?>
<?php include("sidebar.php"); ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Administración de Trailers</h1>
                </div>
                <!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item">
                            <a href="Vistapanel.php">Inicio</a>
                        </li>
                        <li class="breadcrumb-item active">Trailers</li>
                    </ol>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- Small boxes (Stat box) ELIMINADO -->
            <!-- /.row -->

            <!--Formulario de productos-->
            <div class="row">
                <div class="col-md-4">
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Registro de Trailers / <small>Trailers</small></h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form class="form-horizontal">
                            <div class="card-body">
                                <div class="form-group row">
                                    <label for="inputNameTrailer" class="col-12">Pelicula</label>
                                    <input type="text" name="nameGender" class="form-control col-8 col-md-6 col-xl-8" id="inputNameTrailer" placeholder="Nombre de Pelicula" disabled>
                                    <button type="button" class="btn btn-success col-4 col-md-6 col-xl-4" data-toggle="modal" data-target="#modal-pelicula">
                                        Seleccione
                                    </button>
                                </div>
                                <div class="form-group row">
                                    <label for="inputNameTableURL">Alojamiento</label>
                                    <input type="text" name="nameGender" class="form-control" id="inputNameTableURL" placeholder="URL del trailer">
                                </div>

                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Agregar</button>
                                <button type="submit" class="btn btn-default float-right">
                                    Limpiar
                                </button>
                            </div>
                            <!-- /.card-footer -->
                        </form>
                    </div>
                </div>

                <!--modal SELECCIONAR PELICULA-->
                <div class="modal fade" id="modal-pelicula">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Listado de Peliculas</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <!-- /.card-header -->
                                <div class="card-body table-responsive p-0" style="height: 485px;">
                                    <table class="table table-head-fixed text-nowrap">
                                        <thead>
                                            <tr>
                                                <th>Seleccione</th>
                                                <th>ID</th>
                                                <th>Nombre</th>
                                                <th>Idioma</th>
                                                <th>Calidad</th>
                                                <th>Fecha Estreno</th>
                                                <th>Foto</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="text-center">
                                                    <input type="radio" name="radPelicula" id="radioNoLabel1" value="" aria-label="...">
                                                </td>
                                                <td>1</td>
                                                <td>IT-ESO</td>
                                                <td>Español</td>
                                                <td>HD</td>
                                                <td>05-09-2017</td>
                                                <td>
                                                    <img src="https://i0.wp.com/lascronicasdedeckard.com/wp-content/uploads/2019/09/It-2017-Las-Cronicas-de-Deckard-Portada.jpg?w=830&ssl=1" alt="IT - ESO" style="width: 70px; height: 100px;">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-center">
                                                    <input class="" type="radio" name="radPelicula" id="radioNoLabel1" value="radPelicula" aria-label="...">
                                                </td>
                                                <td>2</td>
                                                <td>IT-ESO 2</td>
                                                <td>Español</td>
                                                <td>FULL HD</td>
                                                <td>26-08-2019</td>
                                                <td>
                                                    <img src="https://as01.epimg.net/epik/imagenes/2019/09/05/portada/1567689486_408333_1567689625_noticia_normal.jpg" alt="IT - ESO 2" style="width: 70px; height: 100px;">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-center">
                                                    <input class="" type="radio" name="radPelicula" id="radioNoLabel1" value="" aria-label="...">
                                                </td>
                                                <td>3</td>
                                                <td>La mascara</td>
                                                <td>Español</td>
                                                <td>360p</td>
                                                <td>28-07-1994</td>
                                                <td>
                                                    <img src="https://images.uncyclomedia.co/inciclopedia/es/thumb/8/8d/La-mascara-portada-original.jpg/250px-La-mascara-portada-original.jpg" alt="La Mascara" style="width: 70px; height: 100px;">
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.card-body -->
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /.modal SELECCIONAR PELICULA-->

                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Listado de Trailers</h3>

                            <div class="card-tools">
                                <div class="input-group input-group-sm">
                                    <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

                                    <div class="input-group-append">
                                        <button type="submit" class="btn btn-default">
                                            <i class="fas fa-search"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body table-responsive p-0" style="height: 485px;">
                            <table class="table table-head-fixed text-nowrap">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Pelicula</th>
                                        <th>Alojamiento del trailer</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>IT-ESO</td>
                                        <td>
                                            <a href="https://www.youtube.com/embed/_oBZ_zTz0Nw" target="_blank" rel="noopener noreferrer">https://www.youtube.com/embed/_oBZ_zTz0Nw</a>
                                        </td>
                                        <td>
                                            <form action="" method="POST" class="m-0">
                                                <input type="hidden" name="nameGender">
                                                <button type="submit" class="btn btn-warning" value="btnModificar" name="action">Modificar</button>
                                                <button type="submit" class="btn btn-danger" value="btnEliminar" name="action">Eliminar</button>
                                            </form>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>IT-ESO 2</td>
                                        <td>
                                            <a href="https://www.youtube.com/embed/o1sQbtZpsic" target="_blank" rel="noopener noreferrer">https://www.youtube.com/embed/o1sQbtZpsic</a>
                                        </td>
                                        <td>
                                            <form action="" method="POST" class="m-0">
                                                <input type="hidden" name="nameGender">
                                                <button type="submit" class="btn btn-warning" value="btnModificar" name="action">Modificar</button>
                                                <button type="submit" class="btn btn-danger" value="btnEliminar" name="action">Eliminar</button>
                                            </form>
                                        </td>
                                    <tr>
                                        <td>2</td>
                                        <td>La Mascara</td>
                                        <td>
                                            <a href="https://www.youtube.com/embed/DhP0P4q8jLk" target="_blank" rel="noopener noreferrer">https://www.youtube.com/embed/DhP0P4q8jLk</a>
                                        </td>
                                        <td>
                                            <form action="" method="POST" class="m-0">
                                                <input type="hidden" name="nameGender">
                                                <button type="submit" class="btn btn-warning" value="btnModificar" name="action">Modificar</button>
                                                <button type="submit" class="btn btn-danger" value="btnEliminar" name="action">Eliminar</button>
                                            </form>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
            <!--Fin de formulario de productos-->


        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!--FOOTER-->
<?php include("footer.php") ?>

<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
</aside>
<!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge("uibutton", $.ui.button);
</script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- ChartJS -->
<script src="plugins/chart.js/Chart.min.js"></script>
<!-- Sparkline -->
<!-- <script src="plugins/sparklines/sparkline.js"></script> ELIMINADO-->
<!-- JQVMap -->
<!-- <script src="plugins/jqvmap/jquery.vmap.min.js"></script> ELIMINADO-->
<!-- <script src="plugins/jqvmap/maps/jquery.vmap.usa.js"></script>ELIMINADO -->
<!-- jQuery Knob Chart -->
<script src="plugins/jquery-knob/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="plugins/moment/moment.min.js"></script>
<script src="plugins/daterangepicker/daterangepicker.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Summernote -->
<script src="plugins/summernote/summernote-bs4.min.js"></script>
<!-- overlayScrollbars -->
<script src="plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.js"></script>
<!-- <script src="dist/js/demo.js"></script> ELIMINADO -->
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!-- <script src="dist/js/pages/dashboard.js"></script> -->
<!--jquery validation-->
<script src="plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="plugins/jquery-validation/additional-methods.min.js"></script>

<script>
    document.getElementById("pelicula-menu").className = "nav-item menu-open";
    document.getElementById("adminpelicula").className = "nav-link active";
    document.getElementById("trailer").className = "nav-link active";
    $(function() {
        $.validator.setDefaults({
            submitHandler: function() {
                alert("Form successful submitted!");
            }
        });
        $('#quickForm').validate({
            rules: {
                nameLanguage: {
                    required: true,
                },
            },
            messages: {
                nameLanguage: {
                    required: "Porfavor ingresa el Idioma"
                },
            },
            errorElement: 'span',
            errorPlacement: function(error, element) {
                error.addClass('invalid-feedback');
                element.closest('.form-group').append(error);
            },
            highlight: function(element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).removeClass('is-invalid');
            }
        });
    });
</script>
</body>

</html>