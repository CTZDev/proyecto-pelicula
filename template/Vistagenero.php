<?php include("modulos/genero.php") ?>

<?php include("cabecera.php"); ?>
<?php include("sidebar.php"); ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Administración de Género</h1>
                </div>
                <!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item">
                            <a href="Vistapanel.php">Inicio</a>
                        </li>
                        <li class="breadcrumb-item active">Genero</li>
                    </ol>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- Small boxes (Stat box) ELIMINADO -->
            <!-- /.row -->

            <!--Formulario de productos-->
            <div class="row">
                <div class="col-md-4">
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Registro de Genero / <small>Gender</small></h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form id="typeUserForm" class="form-horizontal" method="POST">
                            <div class="card-body">
                                <div class="form-group row">
                                    <input type="hidden" required class="form-control" id="txtID" name="txtID"
                                        placeholder="Tipo de Usuario" autocomplete="off" value="<?php echo $txtID ?>">
                                    <label for="txtName">Nombre</label>
                                    <input type="text" required class="form-control" id="txtName" name="txtName"
                                        placeholder="Nombre del Genero" autocomplete="off"
                                        value="<?php echo $txtName ?>">
                                </div>

                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button value="btnAgregar" <?php echo $accionAgregar ?> type="submit"
                                    class="btn btn-primary" name="option">Agregar</button>
                                <button value="btnCancelar" <?php echo $accionCancelar  ?> type="submit"
                                    class="btn btn-primary float-right" name="option">Cancelar</button>
                                <button value="btnModificar" <?php echo $accionModificar ?> type="submit"
                                    class="btn btn-warning float-right" name="option">Modificar</button>
                            </div>
                            <!-- /.card-footer -->
                        </form>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Listado de Generos</h3>

                            <div class="card-tools">
                                <div class="input-group input-group-sm">
                                    <input type="text" name="table_search" class="form-control float-right"
                                        placeholder="Search">

                                    <div class="input-group-append">
                                        <button type="submit" class="btn btn-default">
                                            <i class="fas fa-search"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body table-responsive p-0" style="height: 485px;">
                            <table class="table table-head-fixed text-nowrap">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Nombre</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($data as $val) { ?>
                                    <tr>
                                        <td><?php echo $val['idgenero']; ?></td>
                                        <td><?php echo $val['nombre']; ?></td>
                                        <td>
                                            <form action="" method="POST">
                                                <input type="hidden" name="txtID"
                                                    value="<?php echo $val['idgenero']; ?>">
                                                <input type="hidden" name="txtName"
                                                    value="<?php echo $val['nombre']; ?>">
                                                <input type="submit" value="Seleccionar Registro" name="option"
                                                    class="btn btn-info">
                                                <button value="btnEliminar"
                                                    onclick="return Confirmar('¿Realmente deseas borrar el registro?');"
                                                    type="submit" class="btn btn-danger" name="option">Eliminar</button>
                                            </form>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
            <!--Fin de formulario de productos-->


        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!--FOOTER-->
<?php include("footer.php") ?>
<script>
    document.getElementById("genero").className = "nav-link active";
</script>
<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
</aside>
<!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
$.widget.bridge("uibutton", $.ui.button);
</script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- ChartJS -->
<script src="plugins/chart.js/Chart.min.js"></script>
<!-- Sparkline -->
<!-- <script src="plugins/sparklines/sparkline.js"></script> ELIMINADO-->
<!-- JQVMap -->
<!-- <script src="plugins/jqvmap/jquery.vmap.min.js"></script> ELIMINADO-->
<!-- <script src="plugins/jqvmap/maps/jquery.vmap.usa.js"></script>ELIMINADO -->
<!-- jQuery Knob Chart -->
<script src="plugins/jquery-knob/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="plugins/moment/moment.min.js"></script>
<script src="plugins/daterangepicker/daterangepicker.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Summernote -->
<script src="plugins/summernote/summernote-bs4.min.js"></script>
<!-- overlayScrollbars -->
<script src="plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.js"></script>
<!-- <script src="dist/js/demo.js"></script> ELIMINADO -->
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!-- <script src="dist/js/pages/dashboard.js"></script> -->
<!--jquery validation-->
<script src="plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="plugins/jquery-validation/additional-methods.min.js"></script>

<script>
function Confirmar(mensaje) {
    return (confirm(mensaje)) ? true : false
}
</script>
</body>

</html>