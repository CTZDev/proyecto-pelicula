    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
        <!-- Brand Logo -->
        <a href="#" class="brand-link">
            <img src="dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: 0.8" />
            <span class="brand-text font-weight-light">Sistema Peliculas</span>
        </a>

        <!-- Sidebar -->
        <div class="sidebar">
            <!-- Sidebar user panel (optional) -->
            <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                <div class="image">
                    <img src="dist/img/cnMovie.png" class="img-circle elevation-2" alt="User Image" width="2.1rem" style="height: 2rem !important;" />
                </div>
                <div class="info">
                    <a href="#" class="d-block">CN&MOVIE</a>
                </div>
            </div>

            <!-- SidebarSearch Form -->
            <div class="form-inline">
                <div class="input-group" data-widget="sidebar-search">
                    <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search" />
                    <div class="input-group-append">
                        <button class="btn btn-sidebar">
                            <i class="fas fa-search fa-fw"></i>
                        </button>
                    </div>
                </div>
            </div>

            <!-- Sidebar Menu -->
            <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                    <!-- Add icons to the links using the .nav-icon class
                    with font-awesome or any other icon font library -->
                    <li class="nav-item">
                        <a href="Vistapanel.php" class="nav-link" id="inicio">
                            <i class="nav-icon active fas fa-ellipsis-h"></i>
                            <p>Inicio</p>
                        </a>
                    </li>
                    <li class="nav-header">ADM. USUARIOS</li>
                    <li class="nav-item">
                        <a href="Vistausuarios.php" class="nav-link" id="usuario">
                            <i class="nav-icon fas fa-ellipsis-h"></i>
                            <p>Usuarios</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="Vistatipos_usuario.php" class="nav-link" id="tipousuario">
                            <i class="nav-icon fas fa-file"></i>
                            <p>Tipos de Usuario</p>
                        </a>
                    </li>
                    <li class="nav-header">ADM. PELICULA</li>
                    <li class="nav-item">
                        <a href="Vistagenero.php" class="nav-link" id="genero">
                            <i class="nav-icon far fa-calendar-alt"></i>
                            <p>Genero</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="Vistapais_origen.php" class="nav-link" id="pais">
                            <i class="nav-icon far fa-image"></i>
                            <p>Pais de origen</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="Vistaidioma.php" class="nav-link" id="idioma">
                            <i class="nav-icon fas fa-columns"></i>
                            <p>Idioma</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="Vistacalidad.php" class="nav-link" id="calidad">
                            <i class="nav-icon fas fa-columns"></i>
                            <p>Calidad</p>
                        </a>
                    </li>
                    <li class="nav-item" id="participante-menu">
                        <a href="#" class="nav-link" id="adminparticipante">
                            <i class="nav-icon far fa-envelope"></i>
                            <p>
                                Adm. Participantes
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="Vistaparticipante.php" class="nav-link" id="participante">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Participante</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="Vistatipo_participante.php" class="nav-link" id="tipoparticipante">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Tipo de Participante</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item" id="pelicula-menu">
                        <a href="#" class="nav-link" id="adminpelicula">
                            <i class="nav-icon fas fa-book"></i>
                            <p>
                                Peliculas
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="Vistaregistrar_pelicula.php" class="nav-link" id="pelicula">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>
                                        Registro de Peliculas
                                        <span class="badge badge-primary right">R</span>
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="Vistaconsultar_pelicula.php" class="nav-link" id="lpelicula">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>
                                        Listado de Peliculas
                                        <span class="badge badge-success right">L</span>
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="Vistatrailer.php" class="nav-link" id="trailer">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Trailers</p>
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="nav-header">VALORACION DE PELICULA</li>
                    <li class="nav-item">
                        <a href="Vistaconsulta_vistas.php" class="nav-link" id="vista">
                            <i class="nav-icon fas fa-ellipsis-h"></i>
                            <p>Consulta de Vistas</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="Vistaconsulta_favoritos.php" class="nav-link" id="favorito">
                            <i class="nav-icon fas fa-file"></i>
                            <p>Consulta de Favoritos</p>
                        </a>
                    </li>
                </ul>
            </nav>
            <!-- /.sidebar-menu -->
        </div>
        <!-- /.sidebar -->
    </aside>